#!/bin/bash

VERSION=$1

docker build -f Dockerfile --tag pauval99/hacker-news_web:${VERSION} .
docker build -f Dockerfile --tag pauval99/hacker-news_web:latest .
docker push pauval99/hacker-news_web:${VERSION}
docker push pauval99/hacker-news_web:latest

docker build -f DockerfileNginx --tag pauval99/hacker-news_nginx:${VERSION} .
docker build -f DockerfileNginx --tag pauval99/hacker-news_nginx:latest .
docker push pauval99/hacker-news_nginx:${VERSION}
docker push pauval99/hacker-news_nginx:latest