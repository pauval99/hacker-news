FROM php:7.4.8-fpm

RUN apt update \
    && apt install -y \
        nano \
        sudo \
        curl \
        zip \
        unzip \
        npm \
    && apt autoremove -y \
    && apt clean \
    && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && curl https://www.npmjs.com/install.sh | sh

RUN groupadd -g 1000 hacker-news \
    && useradd -u 1000 -ms /bin/bash -g hacker-news hacker-news

USER hacker-news

COPY --chown=hacker-news:hacker-news ./web/ /web

WORKDIR /web

RUN composer install \
    && chmod 755 -R /web/storage \
    && php artisan key:generate

EXPOSE 9000
CMD ["php-fpm"]