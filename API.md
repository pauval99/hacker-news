# Contributions

+ **GET /contributions** *obtener todas las contribuciones*
+ **POST /contributions** *crear una contribucion*
+ **GET /contributions/:id** *obtener una contribucion por id*
+ **PUT /contributions/:id** *actualizar una contribucion*
+ **DELETE /contributions/:id** *eliminar una contribucion*
+ **GET /contributions/types/:type** *obterener todas las contribudiones de tipo :type*
+ **GET /contributions/:id/comments** *obtener todos los comentarios de una contribucion*
+ **POST /contributions/:id/comments** *añadir un comentario a la contribucion*
+ **POST /contributions/:id/votes** *añadir un voto a la contribucion*
+ **DELETE /contributions/:id/votes** *borrar un voto de una contribucion*

### JSON
```json
{
    "id": 3,
    "title": "Contribution Title",
    "url": "http://im.a.url",
    "text": "I'm text",
    "type": "url",
    "score": 12,
    "commentsCount": 3,
    "status": "owner",
    "user": {
        "id": 5,
        "username": "TestName"
    },
    "created_at": "2020-11-24T10:56:35.000000Z",
    "updated_at": "2020-11-24T10:56:35.000000Z"
}
```

# Comments

+ **GET /comments/:id** *obtener un comentario*
+ **PUT /comments/:id** *actualizar un comentario*
+ **DELETE /comments/:id** *eliminar un comentario*
+ **GET /comments/:id/comments** *obtener todos las replicas de un comentario*
+ **POST /comments/:id/comments** *añadir una replica a un comentario*
+ **POST /comments/:id/votes** *añadir un voto a un comentario*
+ **DELETE /comments/:id/votes** *borrar un voto de un comentario*

### JSON
```json
{
    "id": 5,
    "user": {
        "id": 5,
        "username": "TestName"
    },
    "text": "I'm text",
    "score": 12,
    "status": "voted",
    "commentsCount": 3,
    "parent_contribution_id": 25,
    "parent_comment_id": 53,
    "created_at": "2020-11-24T10:56:35.000000Z",
    "updated_at": "2020-11-24T10:56:35.000000Z"
}
```

# Users

+ **GET /users/:id** *obtener un usuario*
+ **PUT /users/:id** *actualizar la informacion de un usuario*
+ **GET /users/:id/comments** *obtener todos los comentarios de un usuario*
+ **GET /users/:id/contributions** *obtener todas las contribuciones de un usuario*
+ **GET /users/:id/voted-contributions** *obtener todas las contribuciones votadas por un usuario*
+ **GET /users/:id/voted-comments** *obtener todos los comentarios votados por un usuario*

### JSON
```json
{
    "id": 5,
    "email": "TestMail@gmail.com",
    "username": "TestName",
    "karma": 7,
    "created_at": "2020-11-24T10:56:35.000000Z",
    "updated_at": "2020-11-24T10:56:35.000000Z"
}
```