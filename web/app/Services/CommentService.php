<?php
namespace App\Services;

use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class CommentService 
{
    public static function delete(Comment $comment) {
        foreach($comment->childComments as $childComment)
            self::delete($childComment);

        DB::table('user_comment_votes')->where(['comment_id' => $comment->id])->delete();
        $comment->delete();
    }

    public static function getAllComments(Comment $comment) {
        if(empty($comment->childComments))
            return [];

        $result = [];
        foreach($comment->childComments as $childComment) {
            $arrayChildComment = (new CommentResource($childComment))->toArray(null); 
            $arrayChildComment['comments'] = self::getAllComments($childComment);
            $result[] = $arrayChildComment;
        }
        
        return $result;
    }

    public static function getStatus(Comment $comment, User $user) {
        if($comment->user->id == $user->id)
            return Comment::STATUS_OWNER;
        if($user->votedComment($comment))
            return Comment::STATUS_VOTED;
        return Comment::STATUS_UNVOTED;
    }
}