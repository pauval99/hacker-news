<?php
namespace App\Services;

use App\Http\Resources\CommentResource;
use App\Models\Contribution;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ContributionService 
{
    public static function delete(Contribution $contribution) {
        foreach($contribution->childComments as $childComment)
            CommentService::delete($childComment);

        DB::table('user_contribution_votes')->where(['contribution_id' => $contribution->id])->delete();
        $contribution->delete();
    }

    public static function getAllComments(Contribution $contribution) {
        $result = [];
        foreach($contribution->childComments as $childComment) {
            $arrayChildComment = (new CommentResource($childComment))->toArray(null); 
            $arrayChildComment['comments'] = CommentService::getAllComments($childComment);
            $result[] = $arrayChildComment;
        }
        return $result;
    }

    public static function getStatus(Contribution $contribution, User $user) {
        if($contribution->user->id == $user->id)
            return Contribution::STATUS_OWNER;
        if($user->votedContribution($contribution))
            return Contribution::STATUS_VOTED;
        return Contribution::STATUS_UNVOTED;
    }
}