<?php

namespace App\Models;

use App\Helpers\DateHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id contribution id.
 * @property string $title contribution title.
 * @property string $url contribution url.
 * @property string $text contribution text.
 * @property string $host contribution url host.
 * @property string $type contribution type.
 * @property int $score contribution score.
 * @property string $elapsedTime contribution elapsed time since creation.
 * @property User $user contribution user.
 * @property int $commentsCount contribution comments count.
 * @property Comment[] $childComments only one deep contribution comments.
 */
class Contribution extends Model
{
    const TYPE_URL = 'url';
    const TYPE_ASK = 'ask';

    const VALID_TYPES = [self::TYPE_URL,self::TYPE_ASK];
 
    const STATUS_OWNER = 'owner';
    const STATUS_VOTED = 'voted';
    const STATUS_UNVOTED = 'unvoted';

    protected $fillable = ['title','url','text'];

    public function getUserAttribute() {
        return User::where('id', $this->attributes['user_id'])->first();
    }

    public function setUserAttribute($user) {
        $this->attributes['user_id'] = $user->id;
    }

    public function getCommentsCountAttribute() {
        return Comment::where('parent_contribution_id', $this->attributes['id'])->count();
    }

    public function getChildCommentsAttribute() {
        return Comment::where('parent_contribution_id', $this->attributes['id'])->whereNull('parent_comment_id')->get();
    }

    public function getScoreAttribute() {
        return DB::table('user_contribution_votes')->where('contribution_id', $this->attributes['id'])->count();
    }

    public function getHostAttribute() {
        return parse_url($this->attributes['url'], PHP_URL_HOST);
    }

    public function getElapsedTimeAttribute() {
        return DateHelper::elapsedTime($this->attributes['created_at']).' ago';
    }
}
