<?php

namespace App\Models;

use App\Helpers\DateHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id comment id.
 * @property User $user comment user.
 * @property string $text comment text.
 * @property int $score comment score.
 * @property string $elapsedTime contribution elapsed time since creation.
 * @property int $commentsCount comment child comments count.
 * @property Contribution $contribution comment contribution parent.
 * @property Comment $comment comment parent comment.
 * @property Comment[] $childComments only one deep chid comments.
 */
class Comment extends Model
{
    const PARENT_CONTRIBUTION = 'contribution';
    const PARENT_COMMENT = 'comment';

    const STATUS_OWNER = 'owner';
    const STATUS_VOTED = 'voted';
    const STATUS_UNVOTED = 'unvoted';

    protected $fillable = ['text'];

    public function getUserAttribute() {
        return User::where('id', $this->attributes['user_id'])->first();
    }

    public function setUserAttribute($user) {
        $this->attributes['user_id'] = $user->id;
    }

    public function getCommentsCountAttribute() {
        return Comment::where('parent_comment_id', $this->attributes['id'])->count();
    }

    public function setContributionAttribute($contribution) {
        $this->attributes['parent_contribution_id'] = $contribution->id;
    }

    public function getContributionAttribute() {
        if($this->attributes['parent_contribution_id'] === NULL)
            return Comment::where('id', $this->attributes['parent_comment_id'])->first()->contribution;
        return Contribution::where('id', $this->attributes['parent_contribution_id'])->first();
    }

    public function getCommentAttribute() {
        return Comment::where('id', $this->attributes['parent_comment_id'])->first();
    }

    public function setCommentAttribute($comment) {
        $this->attributes['parent_comment_id'] = $comment->id;
    }

    public function getChildCommentsAttribute() {
        return Comment::where('parent_comment_id', $this->attributes['id'])->get();
    }

    public function getScoreAttribute() {
        return DB::table('user_comment_votes')->where('comment_id', $this->attributes['id'])->count();
    }

    public function getElapsedTimeAttribute() {
        return DateHelper::elapsedTime($this->attributes['created_at']).' ago';
    }
}
