<?php

namespace App\Models;

use App\Helpers\DateHelper;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id user id.
 * @property string $google_id user google id.
 * @property string $email user email.
 * @property string $username user username.
 * @property string $karma user karma.
 * @property string $elapsedTime contribution elapsed time since creation.
 * @property Contribution[] $votedContributions user voted contributions.
 * @property Comment[] $votedComments user voted comments.
 * @property Comment[] $comments user comments.
 */
class User extends Authenticatable
{
    protected $fillable = ['google_id', 'email', 'username', 'api_token'];

    public function getCommentsAttribute() {
        return Comment::where('user_id', $this->attributes['id'])->get();
    }

    public function getKarmaAttribute() {
        $karma = 0;
        $contributions = Contribution::where('user_id', $this->attributes['id'])->get();
        foreach($contributions as $contribution)
            $karma += $contribution->score;
        $comments = Comment::where('user_id', $this->attributes['id'])->get();
        foreach($comments as $comments)
            $karma += $comments->score;
        return $karma;
    }

    public function getElapsedTimeAttribute() {
        return DateHelper::elapsedTime($this->attributes['created_at']).' ago';
    }

    public function getVotedContributionsAttribute() {
        return Contribution::find(DB::table('user_contribution_votes')->where('user_id', $this->attributes['id'])->pluck('contribution_id')->toArray());
    }

    public function getVotedCommentsAttribute() {
        return Comment::find(DB::table('user_comment_votes')->where('user_id', $this->attributes['id'])->pluck('comment_id')->toArray());
    }

    public function isLoggedUserContribution($contribution)
    {
        return $contribution->user == Auth::user();
    }

    public function votedContribution($contribution) {
        return DB::table('user_contribution_votes')->where('contribution_id', $contribution->id)->where('user_id', $this->attributes['id'])->first() !== NULL;
    }

    public function isLoggedUserComment($comment)
    {
        return $comment->user == Auth::user();
    }

    public function votedComment($comment) 
    {
        return DB::table('user_comment_votes')->where('comment_id', $comment->id)->where('user_id', $this->attributes['id'])->first() !== NULL;
    }
}
