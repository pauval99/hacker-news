<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Services\CommentService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function update(Request $request, $id) {
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($comment->user != auth('api')->user())
            return response()->json(['message' => 'Forbidden.'], 403);
        
        $request->validate([
            'text' => ['required', 'max:65535'],
        ]);

        $comment->update($request->all());
        return new CommentResource($comment);
    }

    public function delete($id) {
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($comment->user != auth('api')->user())
            return response()->json(['message' => 'Forbidden.'], 403);
        
        CommentService::delete($comment);
        return response()->json(['message' => 'Deleted.'], 200);
    }

    public function comments($id) {
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return CommentService::getAllComments($comment);
    }

    public function vote($id) {
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($comment->user == auth('api')->user())
            return response()->json(['message' => 'Can\'t vote or unvote your comments.'], 403);
        if(auth('api')->user()->votedComment($comment))
            return response()->json(['message' => 'Comment already voted.'], 403);
        DB::table('user_comment_votes')->insert(['user_id'=>auth('api')->user()->id, 'comment_id'=>$comment->id]);
        return new CommentResource($comment);
    }

    public function unvote($id) {
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($comment->user == auth('api')->user())
            return response()->json(['message' => 'Can\'t vote or unvote your comments.'], 403);
        if(!auth('api')->user()->votedComment($comment))
            return response()->json(['message' => 'Comment not voted yet.'], 403);
        DB::table('user_comment_votes')->where(['user_id'=>auth('api')->user()->id, 'comment_id'=>$comment->id])->delete();
        return new CommentResource($comment);
    }

    public function store(Request $request, $id)
    {   
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        $request->validate([
            'text' => ['required', 'max:65535'],
        ]);
        $comment = new Comment($request->all());
        $comment->user = auth('api')->user();
        $parentComment = Comment::where('id', $id)->first();
        $comment->comment = $parentComment;
        $comment->contribution = $parentComment->contribution;
        $comment->save();
        return response()->json(new CommentResource($comment), 201);
    }
    
    public function show($id) {
        $comment = Comment::find($id);
        if($comment === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return new CommentResource($comment);
    }
  
}
