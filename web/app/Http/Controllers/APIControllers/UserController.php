<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Models\Contribution;
use App\Http\Resources\CommentResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\ContributionResource;
use App\Models\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function update(Request $request, $id) {
        $user = User::find($id);
        if($user === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if(auth('api')->user() != $user)
            return response()->json(['message' => 'Forbidden.'], 403);
        $request->validate([
            'username' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255']
        ]);
        $user->update($request->all());
        return new UserResource($user);
    }
    
    public function contributions($id) {
        $user = User::find($id);
        if($user === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return ContributionResource::collection(Contribution::where('user_id', $id)->get());
    }

    public function show($id) {
        $user = User::find($id);
        if($user === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return new UserResource($user);
    }

    public function votedContributions($id) {
        $user = User::find($id);
        if($user === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if(auth('api')->user() != $user)
            return response()->json(['message' => 'Forbidden.'], 403);
        return ContributionResource::collection(auth('api')->user()->votedContributions);
    }

    public function votedComments($id) {
        $user = User::find($id);
        if($user === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if(auth('api')->user() != $user)
            return response()->json(['message' => 'Forbidden.'], 403);
        return CommentResource::collection(auth('api')->user()->votedComments);
    }
    public function comments($id) {
        $user = User::find($id);
        if($user === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return CommentResource::collection($user->comments);
    }
}
