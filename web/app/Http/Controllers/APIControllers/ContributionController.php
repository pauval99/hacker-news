<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ContributionResource;
use App\Models\Contribution;
use App\Models\Comment;
use App\Services\ContributionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContributionController extends Controller
{
    public function submit(Request $request){
        $request->validate([
            'title' => ['required', 'max:255'],
            'url' => ['required_without:text', 'nullable', 'max:255', 'active_url'],
            'text' => ['required_without:url', 'nullable', 'max:65535'],
        ]);
        
        $contribution = Contribution::where('url', $request->input('url'))->first();
        if(($request->input('url') !== NULL) and ($contribution !== NULL))
            return new ContributionResource($contribution);

        $contribution = new Contribution($request->all());
        $contribution->user = auth('api')->user();
        if (($contribution->url === NULL) and ($contribution->text !== NULL)) {
            $contribution->type = Contribution::TYPE_ASK;
            $contribution->save();
        } elseif ($contribution->url !== NULL) {
            $contribution->type = Contribution::TYPE_URL;
            $contribution->save();
            if ($contribution->text !== NULL) {
                $comment = new Comment();
                $comment->text = $contribution->text;
                $comment->user = $contribution->user;
                $comment->contribution = $contribution;
                $comment->save();
            }
        }
        return response()->json(new ContributionResource($contribution), 201);
    }

    public function update(Request $request, $id) {
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($contribution->user != auth('api')->user())
            return response()->json(['message' => 'Forbidden.'], 403);
        
        $validations = [
            Contribution::TYPE_URL => [
                'title' => ['required', 'max:255'],
                'url' => ['required', 'max:255', 'active_url'],
            ],
            Contribution::TYPE_ASK => [
                'title' => ['required', 'max:255'],
                'text' => ['required', 'max:65535'],
            ]
        ];
        $request->validate($validations[$contribution->type]);

        $attributes = [
            Contribution::TYPE_URL => [
                'title' => $request->input('title'),
                'url' => $request->input('url'),
            ],
            Contribution::TYPE_ASK => [
                'title' => $request->input('title'),
                'text' => $request->input('text'),
            ]
        ];
        $contribution->update($attributes[$contribution->type]);
        return new ContributionResource($contribution);
    }

    public function delete($id) {
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($contribution->user != auth('api')->user())
            return response()->json(['message' => 'Forbidden.'], 403);
        
        ContributionService::delete($contribution);
        return response()->json(['message' => 'Deleted.'], 200);
    }
    
    public function type($type)
    {
        if (!in_array($type, Contribution::VALID_TYPES))
            return response()->json(['message' => 'Not Found.'], 404);
        $contributions = Contribution::where('type',$type)->get();
        return ContributionResource::collection($contributions);
    }

    public function comments($id) {
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return ContributionService::getAllComments($contribution);
    }

    public function show($id) {
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        return new ContributionResource($contribution);
    }

    public function vote($id) {
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($contribution->user == auth('api')->user())
            return response()->json(['message' => 'Can\'t vote or unvote your contributions.'], 403);
        if(auth('api')->user()->votedContribution($contribution))
            return response()->json(['message' => 'Contribution already voted.'], 403);
        DB::table('user_contribution_votes')->insert(['user_id'=>auth('api')->user()->id, 'contribution_id'=>$contribution->id]);
        return new ContributionResource($contribution);
    }

    public function unvote($id) {
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        if($contribution->user == auth('api')->user())
            return response()->json(['message' => 'Can\'t vote or unvote your contributions.'], 403);
        if(!auth('api')->user()->votedContribution($contribution))
            return response()->json(['message' => 'Contribution not voted yet.'], 403);
        DB::table('user_contribution_votes')->where(['user_id'=>auth('api')->user()->id, 'contribution_id'=>$contribution->id])->delete();
        return new ContributionResource($contribution);
    }

    public function store(Request $request, $id)
    {   
        $contribution = Contribution::find($id);
        if($contribution === NULL)
            return response()->json(['message' => 'Not Found.'], 404);
        $request->validate([
            'text' => ['required', 'max:65535'],
        ]);
        $comment = new Comment($request->all());
        $comment->user = auth('api')->user();
        $comment->contribution = Contribution::where('id', $id)->first();
        $comment->save();
        return response()->json(new CommentResource($comment), 201);
    }
    
    public function contributions() {
        $contributions = Contribution::get();
        return ContributionResource::collection($contributions);
    }
}
