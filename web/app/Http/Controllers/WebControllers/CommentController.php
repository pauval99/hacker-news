<?php

namespace App\Http\Controllers\WebControllers;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Contribution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function threads()
    {
        $user = Auth::user();
        $comments = $user->comments->sortByDesc('created_at');
        return view('comment.index', compact('comments'));
    }

    public function upvoted()
    {
        if(!Auth::check())
            return redirect()->route('/login');
        $comments = Auth::user()->votedComments->sortByDesc('created_at');
        return view('comment.index', compact('comments'));
    }

    public function comment($id)
    {
        $contribution = Contribution::where('id', $id)->first();
        $comments = $contribution->childComments->sortByDesc('created_at');
        return view('comment.comment', ['contribution' => $contribution, 'comments'=>$comments]);
    }

    public function reply($id)
    {
        $comment = Comment::where('id', $id)->first();
        return view('comment.reply', ['comment' => $comment]);
    }

    public function store(Request $request)
    {   
        if(!Auth::check())
            return redirect()->route('/login');
        
        $request->validate([
            'text' => ['required', 'max:65535'],
        ]);
        
        $comment = new Comment($request->all());
        $comment->user = Auth::user();
        if($request->input('parent') == Comment::PARENT_CONTRIBUTION) {
            $comment->contribution = Contribution::where('id', $request->input('id'))->first();
        } elseif($request->input('parent') == Comment::PARENT_COMMENT) {
            $parentComment = Comment::where('id', $request->input('id'))->first();
            $comment->comment = $parentComment;
            $comment->contribution = $parentComment->contribution;
        }
        $comment->save();
        return redirect()->route('/comment', $comment->contribution->id);
    }
    
    public function vote($comment_id) {
        if(!Auth::check())
            return redirect()->route('/login');
        DB::table('user_comment_votes')->insert(['user_id'=>Auth::id(), 'comment_id'=>$comment_id]);
        return redirect()->back();
    }

    public function unvote($comment_id) {
        DB::table('user_comment_votes')->where(['user_id'=>Auth::id(), 'comment_id'=>$comment_id])->delete();
        return redirect()->back();
    }
}
