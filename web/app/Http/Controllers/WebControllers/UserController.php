<?php

namespace App\Http\Controllers\WebControllers;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Contribution;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function submissions($user_id) {
        $contributions = Contribution::where('user_id', $user_id)->orderBy('created_at', 'desc')->get();
        return view('contribution.index', compact('contributions'));
    }

    public function comments($user_id) {
        $comments = Comment::where('user_id', $user_id)->orderBy('created_at', 'desc')->get();
        return view('comment.index', compact('comments'));
    }

    public function show($id)
    {
        $user = User::where('id', $id)->first();
        if($id == Auth::id()) {
            return view('user.edit', ['user' => $user]);
        }
        return view('user.view', ['user' => $user]);
    }

    public function edit(Request $request)
    {
        $user = Auth::user();
        $user->fill($request->all());
        $user->save();
        return view('user.edit', ['user' => $user]);
    }
}
