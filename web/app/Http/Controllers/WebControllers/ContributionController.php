<?php

namespace App\Http\Controllers\WebControllers;

use App\Http\Controllers\Controller;
use App\Models\Contribution;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContributionController extends Controller
{
    public function index()
    {
        $contributions = Contribution::where('type', Contribution::TYPE_URL)->get()->sortByDesc('score');
        return view('contribution.index', compact('contributions'));
    }

    public function new()
    {
        $contributions = Contribution::orderBy('created_at', 'desc')->get();
        return view('contribution.index', compact('contributions'));
    }

    public function ask()
    {
        $contributions = Contribution::where('type', Contribution::TYPE_ASK)->orderBy('created_at', 'desc')->get();
        return view('contribution.index', compact('contributions'));
    }

    public function upvoted()
    {
        if(!Auth::check())
            return redirect()->route('/login');
        $contributions = Auth::user()->votedContributions->sortByDesc('created_at');
        return view('contribution.index', compact('contributions'));
    }

    public function submit()
    {
        return view('contribution.submit');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'max:255'],
            'url' => ['required_without:text', 'nullable', 'max:255', 'active_url'],
            'text' => ['required_without:url', 'nullable', 'max:65535'],
        ]);
        
        if(!Auth::check())
            return redirect()->route('/login');
        
        $contribution = Contribution::where('url', $request->input('url'))->first();
        if(($request->input('url') !== NULL) and ($contribution !== NULL))
            return redirect()->route('/comment', $contribution->id);
        
        $contribution = new Contribution($request->all());
        $contribution->user = Auth::user();
        if (($contribution->url === NULL) and ($contribution->text !== NULL)) {
            $contribution->type = Contribution::TYPE_ASK;
            $contribution->save();
        } elseif ($contribution->url !== NULL) {
            $contribution->type = Contribution::TYPE_URL;
            $contribution->save();
            if ($contribution->text !== NULL) {
                $comment = new Comment();
                $comment->text = $contribution->text;
                $comment->user = $contribution->user;
                $comment->contribution = $contribution;
                $comment->save();
            }
        }
        
        return redirect()->route('/contributions/new');
    }

    public function vote($contribution_id) {
        if(!Auth::check())
            return redirect()->route('/login');
        DB::table('user_contribution_votes')->insert(['user_id'=>Auth::id(), 'contribution_id'=>$contribution_id]);
        return redirect()->back();
    }

    public function unvote($contribution_id) {
        DB::table('user_contribution_votes')->where(['user_id'=>Auth::id(), 'contribution_id'=>$contribution_id])->delete();
        return redirect()->back();
    }
}
