<?php

namespace App\Http\Resources;

use App\Models\Comment;
use App\Services\CommentService;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'score' => $this->score,
            'user' => [
                'id' => $this->user->id,
                'username' => $this->user->username,
            ],
            'commentsCount' => $this->commentsCount,
            'status' => CommentService::getStatus(Comment::find($this->id), auth('api')->user()),
            'parent_contribution' => [
                'id' => $this->contribution->id,
                'title' => $this->contribution->title,
            ],
            'parent_comment_id' => $this->parent_comment_id, 
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
