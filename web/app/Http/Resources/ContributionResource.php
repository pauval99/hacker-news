<?php

namespace App\Http\Resources;

use App\Models\Contribution;
use App\Services\ContributionService;
use Illuminate\Http\Resources\Json\JsonResource;

class ContributionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'url' => $this->url,
            'text' => $this->text,
            'type' => $this->type,
            'score' => $this->score,
            'commentsCount' => $this->commentsCount,
            'status' => ContributionService::getStatus(Contribution::find($this->id), auth('api')->user()),
            'user' => [
                'id' => $this->user->id,
                'username' => $this->user->username,
            ],
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
