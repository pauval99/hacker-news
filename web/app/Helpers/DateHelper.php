<?php

namespace App\Helpers;

class DateHelper {
    public static function elapsedTime($date){
        $date = time() - strtotime($date);
        $date = ($date<1)? 1 : $date;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
    
        foreach ($tokens as $unit => $text) {
            if ($date < $unit) continue;
            $numberOfUnits = floor($date / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }
    } 
}