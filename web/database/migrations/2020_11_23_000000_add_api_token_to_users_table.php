<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AddAPITokenToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('api_token', 80)->unique()->nullable()->default(null);
        });

        foreach(User::get() as $user) {
            $user->api_token = Str::random(60);
            $user->save();
        }

        Schema::table('users', function ($table) {
            $table->string('api_token')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('users', 'api_token');
    }
}
