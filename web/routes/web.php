<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\WebControllers\ContributionController;
use App\Http\Controllers\WebControllers\AuthController;
use App\Http\Controllers\WebControllers\CommentController;
use App\Http\Controllers\WebControllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/contributions');

//Contributions
Route::get('/contributions', [ ContributionController::class, 'index' ])->name('/contributions');
Route::get('/contributions/new', [ ContributionController::class, 'new' ])->name('/contributions/new');
Route::get('/contributions/submit', [ ContributionController::class, 'submit' ])->name('/contributions/submit');
Route::get('/contributions/upvoted', [ ContributionController::class, 'upvoted' ])->name('/contributions/upvoted');
Route::post('/contributions/store', [ ContributionController::class, 'store' ])->name('/contributions/store');
Route::get('/contributions/ask', [ ContributionController::class, 'ask' ])->name('/contributions/ask');
Route::get('/contribution/vote/{contribution_id}', [ ContributionController::class, 'vote' ])->name('/contribution/vote');
Route::get('/contribution/unvote/{contribution_id}', [ ContributionController::class, 'unvote' ])->name('/contribution/unvote');

//Comments
Route::get('/comments/threads', [ CommentController::class, 'threads' ])->name('/comments/threads');
Route::get('/comments/upvoted', [ CommentController::class, 'upvoted' ])->name('/comments/upvoted');
Route::get('/comment/{id}', [ CommentController::class, 'comment' ])->name('/comment');
Route::get('/reply/{id}', [ CommentController::class, 'reply' ])->name('/reply');
Route::post('/comments/store', [ CommentController::class, 'store' ])->name('/comments/store');
Route::get('/comment/vote/{comment_id}', [ CommentController::class, 'vote' ])->name('/comment/vote');
Route::get('/comment/unvote/{comment_id}', [ CommentController::class, 'unvote' ])->name('/comment/unvote');

//Profile
Route::get('/user/{id}', [ UserController::class, 'show' ])->name('/user');
Route::post('/user/edit', [ UserController::class, 'edit' ])->name('/user/edit');
Route::get('/user/submissions/{user_id}', [ UserController::class, 'submissions' ])->name('/user/submissions');
Route::get('/user/comments/{user_id}', [ UserController::class, 'comments' ])->name('/user/comments');

//Login
Route::get('/login', [AuthController::class, 'redirectToProvider'])->name('/login');
Route::get('/login/callback', [AuthController::class, 'handleProviderCallback']);
Route::get('/logout', [ AuthController::class, 'logout' ])->name('/logout');

