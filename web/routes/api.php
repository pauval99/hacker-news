<?php

use App\Http\Controllers\APIControllers\CommentController;
use App\Http\Controllers\APIControllers\ContributionController;
use App\Http\Controllers\APIControllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::fallback( function() {
    return response()->json(['message' => 'Not Found.'], 404); 
});

Route::middleware('auth:api')->group(function () {
    //Contributions
    Route::get('/contributions', [ ContributionController::class, 'contributions' ]);
    Route::post('/contributions', [ ContributionController::class, 'submit' ]);
    
    Route::get('/contributions/{id}', [ ContributionController::class, 'show' ]);
    Route::put('/contributions/{id}', [ ContributionController::class, 'update' ]);
    Route::delete('/contributions/{id}', [ ContributionController::class, 'delete' ]);
    
    Route::get('/contributions/types/{type}', [ ContributionController::class, 'type' ]);

    Route::get('/contributions/{id}/comments', [ ContributionController::class, 'comments' ]);
    Route::post('/contributions/{id}/comments', [ ContributionController::class, 'store' ]);
    
    Route::post('/contributions/{id}/votes', [ ContributionController::class, 'vote' ]);
    Route::delete('/contributions/{id}/votes', [ ContributionController::class, 'unvote' ]);
    
    //Comments
    Route::get('/comments/{id}', [ CommentController::class, 'show' ]);
    Route::put('/comments/{id}', [ CommentController::class, 'update' ]);
    Route::delete('/comments/{id}', [ CommentController::class, 'delete' ]);
    
    Route::get('/comments/{id}/comments', [ CommentController::class, 'comments' ]);
    Route::post('/comments/{id}/comments', [ CommentController::class, 'store' ]);

    Route::post('/comments/{id}/votes', [ CommentController::class, 'vote' ]);
    Route::delete('/comments/{id}/votes', [ CommentController::class, 'unvote' ]);

    //User
    Route::get('/users/{id}', [ UserController::class, 'show' ]);
    Route::put('/users/{id}', [ UserController::class, 'update' ]);
    
    Route::get('/users/{id}/contributions', [ UserController::class, 'contributions' ]);
    Route::get('/users/{id}/comments', [ UserController::class, 'comments' ]);

    Route::get('/users/{id}/voted-contributions', [ UserController::class, 'votedContributions' ]);
    Route::get('/users/{id}/voted-comments', [ UserController::class, 'votedComments' ]);
});
