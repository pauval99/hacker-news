@foreach($comments as $comment)
<tr>
    <td>
        <table cellpadding="0" cellspacing="0">
            <tr>
            <td class="ind"><img height="1" width="{{40*$space}}"></td>
                <td>
                    @guest
                        <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                    @endguest
                    @auth
                        @if(!Auth::user()->isLoggedUserComment($comment))
                            @if(!Auth::user()->votedComment($comment))
                                <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                            @else
                                &nbsp&nbsp&nbsp
                            @endif
                        @else
                            <span class="titleUser">&nbsp*&nbsp</span>
                        @endif
                    @endauth
                </td>
                <td>
                    <a class="yclinks" href="{{route("/user", $comment->user->id) }}">{{$comment->user->username}}</a>
                    <span class = "yclinks"></span>
                    <span class="yclinks">{{ $comment->elapsedTime }}</span>
                    @auth
                        @if(!Auth::user()->isLoggedUserComment($comment) and Auth::user()->votedComment($comment))
                            <span class = "subtext">|</span>
                            <a class="subtext" href= "{{ route('/comment/unvote', $comment->id) }}">unvote</a>
                        @endif
                    @endauth
                    <span class = "yclinks">| on:</span>
                    <a class="yclinks" href="{{ route("/comment",['id'=>$comment->contribution->id]) }}"> {{$comment->contribution->title}}</a>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td class="comment">
                {{  $comment->text }}
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td>
                    <a class="subtextB" href="{{ route("/reply",['id'=>$comment->id]) }}">reply</a>
                </td>
            </tr>
        </table>
    </td>
</tr>
@include('comment.recursive', ['comments' => $comment->childComments, 'space' => $space+1])

@endforeach