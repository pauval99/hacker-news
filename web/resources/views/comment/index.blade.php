@extends('layouts.master')

@section('content')
<div class="col">
<table>
    @foreach ($comments as $comment)
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        @guest
                            <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                        @endguest
                        @auth
                            @if(!Auth::user()->isLoggedUserComment($comment))
                                @if(!Auth::user()->votedComment($comment))
                                    <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                                @else
                                    &nbsp&nbsp&nbsp
                                @endif
                            @else
                                <span class="titleUser">&nbsp*&nbsp</span>
                            @endif
                        @endauth
                    </td>
                    <td>
                        <a class="yclinks" href="{{route("/user", $comment->user->id) }}">{{$comment->user->username}}</a>
                        <span class = "yclinks"></span>
                        <span class="yclinks">{{ $comment->elapsedTime }}</span>
                        @auth
                            @if(!Auth::user()->isLoggedUserComment($comment) and Auth::user()->votedComment($comment))
                                <span class = "subtext">|</span>
                                <a class="subtext" href= "{{ route('/comment/unvote', $comment->id) }}">unvote</a>
                            @endif
                        @endauth
                        <span class = "yclinks">| on:</span>
                        <a class="yclinks" href="{{ route("/comment",['id'=>$comment->contribution->id]) }}"> {{$comment->contribution->title}}</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="1"></td>
                    <td class="comment">
                    {{  $comment->text }}
                    </td>
                </tr>
                <tr>
                    <td colspan="1"></td>
                    <td>
                        <a class="subtextB" href="{{ route("/reply",['id'=>$comment->id]) }}">reply</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>        
    <tr id="pagespace" style="height:5px"></tr>
    @endforeach
</table>
</div>

@endsection
