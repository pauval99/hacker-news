@extends('layouts.master')

@section('content')
<div class="col">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                @guest
                    <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                @endguest
                @auth
                    @if(!Auth::user()->isLoggedUserComment($comment))
                        @if(!Auth::user()->votedComment($comment))
                            <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                        @else
                            &nbsp&nbsp&nbsp
                        @endif
                    @else
                        <span class="titleUser">&nbsp*&nbsp</span>
                    @endif
                @endauth
            </td>
            <td>
                <a class="yclinks" href="{{route("/user", $comment->user->id) }}">{{$comment->user->username}}</a>
                <span class = "yclink"></span>
                <a class="yclinks"href= "{{ route("/reply",['id'=>$comment->id]) }}">{{$comment->elapsedTime}}</a>
                @auth
                    @if(!Auth::user()->isLoggedUserComment($comment) and Auth::user()->votedComment($comment))
                        <span class = "subtext">|</span>
                        <a class="subtext" href= "{{ route('/comment/unvote', $comment->id) }}">unvote</a>
                    @endif
                @endauth
                <span class = "yclinks">| on:</span>
                <a class="yclinks" href="{{ route("/comment",['id'=>$comment->contribution->id]) }}"> {{$comment->contribution->title}}</a>
            </td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td class="comment">
                <p>{{$comment->text}}</p>
            </td>
        </tr>
    </table>
</div>
<div class="col-6">
    <form action="{{ route('/comments/store') }}" method="POST" >
        @csrf
        <div class="form-group">
            <textarea class="form-control" name="text" rows="4"></textarea>
            @error('text')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <input name="parent" type="hidden" value="{{ App\Models\Comment::PARENT_COMMENT }}">
        <input name="id" type="hidden" value="{{ $comment->id }}">
        <button type="submit" class="btn btn-primary-mine">reply</button>
    </form>
</div>

@endsection