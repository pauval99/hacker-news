@extends('layouts.master')

@section('content')
<div class="col-10">
    <table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            @guest
                <a class="title" id="up_{{ $contribution->id }}" href={{route('/contribution/vote', $contribution->id)}}> ▲ </a>
            @endguest
            @auth
                @if(!Auth::user()->isLoggedUserContribution($contribution))
                    @if(!Auth::user()->votedContribution($contribution))
                        <a class="title" id="up_{{ $contribution->id }}" href={{route('/contribution/vote', $contribution->id)}}> ▲ </a>
                    @else
                        &nbsp &nbsp
                    @endif
                @else
                    <span class="titleUser">&nbsp*&nbsp</span>
                @endif
            @endauth
        </td>
        <td class="title">
        <a class="link" href="{{ $contribution->type == App\Models\Contribution::TYPE_URL ? $contribution->url : route('/comment', $contribution->id) }}">{{$contribution->title}}</a>
            @if($contribution->host !== NULL)
                <span class="yclinks">({{ $contribution->host }})
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="1"></td>
        <td class="subtext">
            <span class="subtext">{{$contribution->score}} points  by </span>
        <a class="subtext" href="{{route("/user", $contribution->user_id) }}">{{$contribution->user->username}}</a>
            <a class="subtext"href= "{{ route("/comment",['id'=>$contribution->id]) }}">{{$contribution->elapsedTime}}</a>
            @auth
                @if(!Auth::user()->isLoggedUserContribution($contribution) and Auth::user()->votedContribution($contribution))
                    <span class = "subtext">|</span>
                    <a class="subtext" href= "{{ route('/contribution/unvote', $contribution->id) }}">unvote</a>
                @endif
            @endauth
            <span class = "subtext">|</span>
            <a class="subtext" href="{{ route("/comment",['id'=>$contribution->id]) }}">{{ $contribution->commentsCount }} comments</a>
        </td>
    </tr>
    <tr>
        @if($contribution->type == App\Models\Contribution::TYPE_ASK)
            <td colspan="1"></td>    
            <td><p class="title">{{ $contribution->text }}</p></td>
        @endif
    </tr>
    </table>
</div>
<div class="col-6">
    <form action="{{ route('/comments/store') }}" method="POST" >
        @csrf
        <div class="form-group">
            <textarea class="form-control" name="text" rows="4"></textarea>
            @error('text')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <input name="parent" type="hidden" value="{{ App\Models\Comment::PARENT_CONTRIBUTION }}">
        <input name="id" type="hidden" value="{{ $contribution->id }}">
        <button type="submit" class="btn btn-primary-mine">add comment</button>
    </form>
</div>
<div id="pagespace" style="height:40px"></div>
<div class="col">
<table>
    @foreach ($comments as $comment)
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        @guest
                            <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                        @endguest
                        @auth
                            @if(!Auth::user()->isLoggedUserComment($comment))
                                @if(!Auth::user()->votedComment($comment))
                                    <a class="title" id="up_{{ $comment->id }}" href={{route('/comment/vote', $comment->id)}}> ▲ </a>
                                @else
                                    &nbsp&nbsp&nbsp
                                @endif
                            @else
                                <span class="titleUser">&nbsp*&nbsp</span>
                            @endif
                        @endauth
                    </td>
                    <td>
                        <a class="yclinks" href="{{route("/user", $comment->user->id) }}">{{$comment->user->username}}</a>
                        <span class = "yclinks"></span>
                        <span class="yclinks">{{ $comment->elapsedTime }}</span>
                        @auth
                            @if(!Auth::user()->isLoggedUserComment($comment) and Auth::user()->votedComment($comment))
                                <span class = "subtext">|</span>
                                <a class="subtext" href= "{{ route('/comment/unvote', $comment->id) }}">unvote</a>
                            @endif
                        @endauth
                    </td>
                </tr>
                <tr>
                    <td colspan="1"></td>
                    <td class="comment">
                    {{  $comment->text }}
                    </td>
                </tr>
                <tr>
                    <td colspan="1"></td>
                    <td>
                        <a class="subtextB" href="{{ route("/reply",['id'=>$comment->id]) }}">reply</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>        
    @include('comment.recursive', ['comments' => $comment->childComments, 'space' => 1])
    <tr id="pagespace" style="height:10px"></tr>
    @endforeach
</table>
</div>

@endsection