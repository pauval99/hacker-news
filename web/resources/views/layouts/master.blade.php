<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <script src="{{ asset('js/web.js')  }}"></script>
    <link rel="stylesheet" href="{{ asset('css/app.css')  }}">
    <link rel="stylesheet" href="{{ asset('css/web.css')  }}">
    <title>HackerNews</title>
</head>
<body>
    <div style="height:10px"></div>
    <div class="col-md">
        <div class="container">
            <nav class="mb-1 nav navbar-expand-lg navbar-light bg-hacker-news">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="navbar-brand" style="font-weight: bold;" href="{{ route('/contributions') }}">Hacker News</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('/contributions/new') }}">News</a></li>
                    @auth
                        <li class="nav-item"><a class="nav-link" href="{{ route('/comments/threads') }}">Threads</a></li>
                    @endauth
                    <li class="nav-item"><a class="nav-link" href="{{ route('/contributions/ask') }}">Ask</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ route('/contributions/submit') }}">Submit</a></li>
                </ul>
                <ul class="navbar-nav ml-auto nav-flex-icons">
                    @guest
                        <li class="nav-item"><a class="nav-link" href="{{ route('/login') }}">Login</a></li>
                    @endguest
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route("/user", Auth::id()) }}">
                                {{ Auth::user()->username }} ({{ Auth::user()->karma }})
                            </a>
                        </li>
                        <li class="nav-item"><a class="nav-link">|</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('/logout') }}">Logout</a></li>
                    @endauth
                </ul>
            </nav>

            @yield('content')
        </div>
    </div>

</body>
</html>