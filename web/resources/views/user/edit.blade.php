@extends('layouts.master')

@section('content')

<div class="col-6">
    <form action="{{ route('/user/edit') }}" method="POST" >
        @csrf
        <table cellpadding="0" cellspacing="0">
            <tbody class="default">
                <tr class="default">
                    <td valign="top">user: </td>
                    <td><input type="text" class="form-control" name="username" size="60" placeholder="Username" value="{{$user->username}}"></a></td>
                </tr>
                <tr class="default">
                    <td valign="top">created: </td>
                    <td><div class="default" >{{$user->elapsedTime}}</div></td>
                </tr>
                <tr class="default">
                    <td valign="top">karma: </td>
                    <td>{{$user->karma}}</td>
                </tr>
                <tr id="pagespace" style="height:3px"></tr>
                <tr>
                    <td valign="top">email: </td>
                <td><input type="text" class="form-control" name="email" size="60" placeholder="Email" value="{{$user->email}}"></td>
                </tr>
                <tr id="pagespace" style="height:3px"></tr>
                <tr>
                    <td style="height: 8px;"></td>
                </tr>
                <tr>
                    <td valign="top">Token: </td>
                    <td><a id="showToken" href="#" class="pagetop" onclick="showToken()"><u>show token</u></a><div id="token" class="pagetop" style="display: none">{{$user->api_token}}</div></td>
                </tr>
                <tr>
                    <td style="height: 12px;"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><a class="pagetop" href="{{ route('/user/submissions', $user->id)}}"><u>submissions</u></a></td>
                </tr>
                <tr>
                    <td></td>
                    <td><a class="pagetop" href="{{ route('/comments/threads') }}"><u>comments</u></a></td>
                </tr>
                <tr>
                    <td></td>
                <td><a class="pagetop" href="{{ route('/contributions/upvoted') }}"><u>upvoted submissions</u></a> / <a class="pagetop" href="{{ route('/comments/upvoted') }}"><u>comments</u></a> (private)</td>
                </tr>
                <tr id="pagespace" style="height:5px"></tr>
                <tr><td></td>
                    <td><button type="submit" class="btn btn-primary-mine">update</button></td></tr>
            </tbody>
        </table>
    </form>
</div>

@endsection