@extends('layouts.master')

@section('content')

<div class="col-4">
    <table cellpadding="0" cellspacing="0">
        <tbody class="default">
            <tr class="default">
                <td valign="top">user:</td>
                <td>
                    <a class="default" href="{{ route("/user", $user->id) }}" class="hnuser">{{$user->username}}
                </td>
            </tr>
            <tr class="default">
                <td valign="top">created:</td>
                <td>
                    <div class="default">{{$user->elapsedTime}}</div>
                </td>
            </tr>
            <tr class="default">
                <td valign="top">karma:</td>
                <td>{{$user->karma}}</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a class="pagetop" href="{{ route('/user/submissions', $user->id)}}"><u>submissions</u></a>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a class="pagetop" href="{{ route('/user/comments', $user->id)}}"><u>comments</u></a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

@endsection