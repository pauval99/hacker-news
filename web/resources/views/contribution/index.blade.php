@extends('layouts.master')

@section('content')

<div class="row">
    <section class="content">
        <div class="col">
            <div class="table-container">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <?php $number = 1; ?>
                        @foreach($contributions as $contribution)
                        <tr id="{{ $contribution->id }}">
                            <td class="title">
                                {{$number++}}.
                            </td>
                            <td>
                                @guest
                                    <a class="title" id="up_{{ $contribution->id }}" href={{route('/contribution/vote', $contribution->id)}}> ▲ </a>
                                @endguest
                                @auth
                                    @if(!Auth::user()->isLoggedUserContribution($contribution))
                                        @if(!Auth::user()->votedContribution($contribution))
                                            <a class="title" id="up_{{ $contribution->id }}" href={{route('/contribution/vote', $contribution->id)}}> ▲ </a>
                                        @else
                                            &nbsp&nbsp&nbsp
                                        @endif
                                    @else
                                        <span class="titleUser">&nbsp*&nbsp</span>
                                    @endif
                                @endauth
                            </td>
                            <td class="title">
                                <a class="link" href="{{ $contribution->type == App\Models\Contribution::TYPE_URL ? $contribution->url : route('/comment', $contribution->id) }}">{{ $contribution->title }}</a>
                                @if($contribution->host !== NULL)
                                    <span class="yclinks">({{ $contribution->host }})
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td class="subtext">
                                <span class="subtext">
                                    {{$contribution->score}} points  by
                                </span>
                                <a class="subtext" href="{{ route("/user", $contribution->user->id) }}">{{$contribution->user->username}}</a>
                                <a class="subtext" href= "{{ route("/comment",['parent'=>App\Models\Comment::PARENT_CONTRIBUTION, 'id'=>$contribution->id]) }}">{{$contribution->elapsedTime}}</a>
                                @auth
                                    @if(!Auth::user()->isLoggedUserContribution($contribution) and Auth::user()->votedContribution($contribution))
                                        <span class = "subtext">|</span>
                                        <a class="subtext" href= "{{ route('/contribution/unvote', $contribution->id) }}">unvote</a>
                                    @endif
                                @endauth
                                <span class = "subtext">|</span>
                                <a class="subtext" href="{{ route("/comment",['id'=>$contribution->id]) }}">{{$contribution->commentsCount}} comments</a>
                            </td>
                        </tr>
                        <tr id="pagespace" style="height:10px"></tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

@endsection