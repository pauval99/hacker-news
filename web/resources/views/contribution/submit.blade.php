@extends('layouts.master')

@section('content')

<div class="col-4"> 
    <form action="{{ route('/contributions/store') }}" method="POST" >
        @csrf
        @foreach (['title','url','text'] as $attribute)
            <div class="form-group">
                <strong>{{ ucfirst($attribute) }}</strong>
                <input type="text" name="{{ $attribute }}" class="form-control" placeholder="{{ ucfirst($attribute) }}">
                @error($attribute)
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        @endforeach
        <button type="submit" class="btn btn-primary-mine">Submit</button>
    </form>
</div>

@endsection