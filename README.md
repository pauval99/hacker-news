### Team
 + Pol Aguilar
 + Sheila Sanchez
 + Sebastian Acurio
 + Pau Val

**URL:** [http://54.204.83.10.xip.io](http://54.204.83.10.xip.io) Down

**Swagger:** [https://app.swaggerhub.com/apis-docs/hacker-news/hacker-news/3.0.1](https://app.swaggerhub.com/apis-docs/hacker-news/hacker-news/3.0.1)

### Exec Web
```sh
sudo apt install docker docker-compose
docker-compose up --build
sudo chmod 755 -R web/storage/
docker exec -it laeraenguara_web composer install
docker exec -it laeraenguara_web php artisan migrate
```